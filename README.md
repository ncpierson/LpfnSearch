# Leapfin Search

Spawns workers to search through random byte streams for the substring "Lpfn" with a default timeout of 60 seconds.

## Usage

### Simple

No building necessary. Requires Java installation (Java 8 or higher).

Run with default options:

    $ bin/lpfnSearch

To see all options:

    $ bin/lpfnSearch -h

If the executable is not working, you can also try using the JAR file:

    $ java -jar bin/lpfnSearch.jar [OPTIONS]

### Advanced

If you want to build the project yourself, you will need to have sbt installed. This codebase was built with
IntelliJ IDEA and you should be able to easily import the project or perform tasks from the command line using sbt.

The final JAR is packaged using [sbt-assembly](https://github.com/sbt/sbt-assembly). To create a fat JAR:

    $ sbt assembly

You should find the JAR at `target/scala-2.12/lpfnSearch.jar`.

Alternatively, to rebuild the JAR and linux executable and include them in the repository, run the build script:

    $ bin/build.sh
