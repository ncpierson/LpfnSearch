name := "LeapfinSearch"

version := "0.1"

scalaVersion := "2.12.4"

libraryDependencies += "org.rogach" %% "scallop" % "3.1.1"

mainClass in assembly := Some("name.nickpierson.LeapfinSearch")

assemblyJarName in assembly := "lpfnSearch.jar"
