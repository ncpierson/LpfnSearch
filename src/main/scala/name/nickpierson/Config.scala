package name.nickpierson

import org.rogach.scallop.ScallopConf

//noinspection TypeAnnotation
class Config(arguments: Seq[String]) extends ScallopConf(arguments) {
  version("Leapfin Search 0.1")
  banner(
    """Usage: lpfnSearch [OPTION]
      |Spawns workers to search for a string in a random stream of bytes and prints the results.
      |Options:
    """.stripMargin
  )

  val timeout = opt[Int](default = Some(60000), descr = "Timeout for each workers in milliseconds")
  val workers = opt[Int](default = Some(10), descr = "Number of workers")
  val needle = opt[String](default = Some("Lpfn"), descr = "The string that each worker tries to find")
  val headers = opt[Boolean](noshort = true, descr = "Adds columns headers to results table")
  val fail = opt[Boolean](descr = "Adds a 50% chance for each worker to fail immediately")
  val help = opt[Boolean](short = 'h', descr = "Shows this help information")
  verify()
}
