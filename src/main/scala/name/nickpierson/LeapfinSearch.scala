package name.nickpierson

import java.util.concurrent.Executors

import name.nickpierson.Search.{SearchFailure, SearchFound, SearchTimeout}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.Random
import scala.util.control.NonFatal

object LeapfinSearch extends App {
  val config = new Config(args)

  val timeout = config.timeout()
  val numWorkers = config.workers()
  val needle = config.needle()

  if (numWorkers <= 0 || numWorkers >= 100) {
    Console.err.println("Workers must be greater than zero and less than 100.")
    sys.exit(1)
  }

  if (needle.isEmpty) {
    Console.err.println("Needle must not be empty string.")
    sys.exit(1)
  }

  if (timeout <= 0) {
    Console.err.println("Timeout must be greater than zero.")
    sys.exit(1)
  }

  // override default thread pool size so that it is neither too small nor too big
  val executor = Executors.newFixedThreadPool(numWorkers)
  implicit val executionContext: ExecutionContext = ExecutionContext.fromExecutor(executor)

  val seekers = List.fill(numWorkers) {
    val fail = config.fail() && Random.nextBoolean()
    Search(needle, timeout, fail)
      .start
      .recover {
        case NonFatal(e) =>
          e.printStackTrace()
          SearchFailure
      }
  }

  val statuses = Await.result(Future.sequence(seekers), Duration.Inf)

  // print header?
  if (config.headers()) {
    printf("%-10s %-10s %-10s\n", "status", "time (ms)", "bytes")
  }

  // print individual reports
  statuses
    .sortBy {
      case SearchFound(elapsed, _) => elapsed
      case SearchTimeout => 0
      case SearchFailure => -1
    }
    .reverse
    .foreach {
      case SearchFound(elapsed, byteCount) => printf("%-10s %-10d %-10d\n", "SUCCESS", elapsed, byteCount)
      case SearchTimeout => printf("%-10s\n", "TIMEOUT")
      case SearchFailure => printf("%-10s\n", "FAILURE")
    }

  // print summary
  val successes = statuses.collect { case s: SearchFound => s }
  val totalBytesRead = successes.foldLeft(0)(_ + _.byteCount)
  val totalSeconds = successes.foldLeft(0L)(_ + _.elapsed) / 1000.0

  if (successes.nonEmpty) {
    printf("Avg Bytes Read Per Second: %d\n", (totalBytesRead / totalSeconds).round)
  }

  // clean up thread pool
  executor.shutdown()
}
