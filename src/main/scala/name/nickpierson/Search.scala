package name.nickpierson

import name.nickpierson.Search._

import scala.annotation.tailrec
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Random

object Search {

  def apply(needle: String, timeout: Int, fail: Boolean)(implicit ec: ExecutionContext): Search = {
    val haystack = if (fail) Iterator.empty else newHaystack
    val searchRequest = SearchRequest(needle, haystack)

    new Search(searchRequest, timeout)
  }

  private def newHaystack = Iterator.continually {
    (Random.nextInt(256) - 128).toByte
  }

  sealed trait SearchStatus

  case class SearchFound(elapsed: Long, byteCount: Int) extends SearchStatus

  case object SearchTimeout extends SearchStatus

  case object SearchFailure extends SearchStatus

  case class SearchRequest(needle: String, haystack: Iterator[Byte])

}

class Search(searchRequest: SearchRequest, timeout: Int)(implicit ec: ExecutionContext) {
  var buffer: List[Char] = List[Char]()
  val bufferLength: Int = searchRequest.needle.length

  var startTime: Long = 0

  def start: Future[SearchStatus] = Future {
    startTime = System.currentTimeMillis()

    search()
  }

  @tailrec
  private def search(byteCount: Int = 1): SearchStatus = {
    val timeSpent = System.currentTimeMillis() - startTime

    if (timeSpent > timeout) return SearchTimeout

    searchRequest match {
      /* Comment out next line to check error handling */
      case SearchRequest(_, haystack) if !haystack.hasNext => SearchFailure
      case SearchRequest(needle, haystack) =>
        val nextChar = haystack.next.toChar

        buffer = (buffer :+ nextChar).takeRight(bufferLength)

        if (buffer.mkString == needle) SearchFound(timeSpent, byteCount)
        else search(byteCount + 1)
    }
  }
}
